
$(document).ready(function(){ 

	// $(".verify-input").keyup(function () {
	// 	if (this.value.length == this.maxLength) {
	// 		$(this).next('input').focus();
	// 	}
	// });


	// $('#yes').on('click', function(){
	// 	$('#submit-btn').prop("disabled", false).removeClass('btn-no-bg');
	// 	var DivToShowHide = document.getElementById("DivToShowHide");
	// 	if(document.getElementById("yes").checked) {
	// 		DivToShowHide.style.opacity = 1 ;
	// 	}
	// })
	// $('#no').on('click', function(){
	// 	$('#submit-btn').prop("disabled", false).removeClass('btn-no-bg');
	// 	var DivToShowHide = document.getElementById("DivToShowHide");
	// 	if(document.getElementById("no").checked) {
	// 		DivToShowHide.style.opacity = 0 ;
	// 	}
	// })

	// admin-staff-slide
	$('.admin-staff-slide').owlCarousel({
		items:4,
		loop:false,
		margin:10,
		nav:false,
		dots: true,
		dotsEach: true,
		// rtl: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			992:{
				items:3
			},
			1000:{
				items:4
			}
		}
	})

	// teacher-staff-slide
	$('.teacher-staff-slide').owlCarousel({
		items:4,
		loop:false,
		margin:10,
		nav:false,
		dots: true,
		dotsEach: true,
		// rtl: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			992:{
				items:3
			},
			1000:{
				items:4
			}
		}
	})

	// technical-staff-slide
	$('.technical-staff-slide').owlCarousel({
		items:4,
		loop:false,
		margin:10,
		nav:false,
		dots: true,
		dotsEach: true,
		// rtl: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			992:{
				items:3
			},
			1000:{
				items:4
			}
		}
	})

	// welcom-slider
	$('.welcom-slider').owlCarousel({
		items:1,
		loop:false,
		margin:0,
		nav:false,
		dots: true,
		dotsEach: true,
		// rtl: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			992:{
				items:1
			},
			1000:{
				items:1
			}
		}
	})

	// img gallery
	// $('.hold-single-img').magnificPopup({
	// 	type:'image',
	// 	delegate: '.single-img',
	// 	image: {
	// 		titleSrc: 'title',
	// 	},
	// 	gallery:{enabled:true}
	// });

	//start popup plugin
	$('.image-g').magnificPopup({
		type:'image',
		// gallery:{enabled:false}
    })
	$('.img-g-page').magnificPopup({
		type:'image',
		// gallery:{enabled:false}
    })

	//start popup plugin
	$('.video-g').magnificPopup({
		type:'iframe',
		// gallery:{enabled:false}
    })
	$('.video-g-page').magnificPopup({
		type:'iframe',
		// gallery:{enabled:false}
    })


	
});




